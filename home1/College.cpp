#include "College.h"

void College::CreateCourse(int num) {
	Course* course = new Course(num);
	collCourses.push_back(course);
}

void College::CreateDep(const char* nullStr) {
	Departament* dep = new Departament(nullStr);
	depts.push_back(dep);
}

void College::CreateStudent(const char* nullStr, const char* lastname, int id, const char* adress_, int start, const char* departament) {
	Student* student = new Student(nullStr, lastname, id, adress_, start);
	Departament* d = SearchDepartament(departament);
	d->depStudents.push_back(student);
	YearFlow* selYear = SearchYear(start);
	if (!selYear) {
		YearFlow* year = new YearFlow(start);
		collYears.push_back(year);
		year->yearFlowStudents.push_back(student);
	}
	else {
		selYear->yearFlowStudents.push_back(student);
	}
}

Student* College::SearchStudent(int num) {
	vector<Departament*>::iterator it;
	vector<Student*>::iterator its;
	for (it = depts.begin(); it != depts.end(); ++it) {
		for (its = ((*it._Ptr)->depStudents).begin(); its != ((*it._Ptr)->depStudents).end(); ++its) {
			if ((*its._Ptr)->id_num == num) {
				return *its;
			}
		}
	}
	throw Exception("Error: No Student Found");
}

YearFlow* College::SearchYear(int num) {
	vector<YearFlow*>::iterator it;
	for (it = collYears.begin(); it != collYears.end(); ++it) {
		if ((*it._Ptr)->start_year == num) {
			return (*it._Ptr);
		}
	}
	return false;
}

Course* College::SearchCourseByNum(int num) {
	vector<Course*>::iterator it;
	for (it = collCourses.begin(); it != collCourses.end(); ++it) {
		if ((*it._Ptr)->course_num == num) {
			return (*it._Ptr);
		}
	}
	throw Exception("Error: No Course Found");
}

Departament* College::SearchDepartament(const char* departament) {
	vector<Departament*>::iterator it;
	for (it = depts.begin(); it != depts.end(); ++it) {
		string word = (*((templateClass*)(*it._Ptr))).get();
		if (word == departament)
		{
			return (*it._Ptr);
		}
	}
	throw Exception("Error: No Departament Found");
}

void College::EditCourse(int num, const char* departament, double score, const char* nullStr) {
	Course* p = SearchCourseByNum(num);
	p->score = score;
	p->ChangeName(nullStr);
	Departament* d = SearchDepartament(departament);
	d->depCourses.push_back(p);
}

void College::AddStudentToCourse(Course* course, Student* student) {
	course->courseStudents.push_back(student);
}

void College::RemoveStudentFromCourse(Course* course, Student* student) {
	vector<Student*>::iterator it;
	for (it = (course->courseStudents.begin()); it != (course->courseStudents.end()); ++it) {
		if (*it == student) {
			course->courseStudents.erase(it);
			break;
		}
	}
}

void College::RemoveStudent(Student* student, int year) {
	vector<Course*>::iterator it;
	for (it = collCourses.begin(); it != collCourses.end(); ++it) {
		RemoveStudentFromCourse(*it._Ptr, student);
	}
	student->end_year = year;
	student->active = false;

}

void College::FinishCourseStudent(Course* course, Student* student) {
	vector<Student*>::iterator it;
	for (it = (course->courseStudents.begin()); it != (course->courseStudents.end()); ++it) {
		if (*it == student) {
			student->nekudot_total = student->nekudot_total + course->score;
			course->courseStudents.erase(it);
			break;
		}
	}
	throw Exception("Error: No Student Found");
}

void College::FinishCourse(Course* course) {
	vector<Student*>::iterator it;
	for (it = (course->courseStudents.begin()); it != (course->courseStudents.end()); ++it) {
		(*it._Ptr)->nekudot_total = (*it._Ptr)->nekudot_total + course->score;
		course->courseStudents.erase(it);
		if (course->courseStudents.empty()) { break; }
	}
}

void College::PrintCourse(Course* course) {
	cout << "Course name: " << course->get() << endl
		<< "Course number: " << course->course_num << endl
		<< "Course score: " << course->score << endl;
	cout << "Students of Course: " << endl;
	vector<Student*>::iterator it;
	for (it = (course->courseStudents.begin()); it != (course->courseStudents.end()); ++it) {
		cout << (*it._Ptr)->get() << " " << (*it._Ptr)->last_name << " " << (*it._Ptr)->id_num << endl;
	}
}

void College::PrintYearFlow(Departament* dep, YearFlow* year) {
	int started = 0;
	int finished = 0;
	vector<Student*>::iterator it;
	vector<Student*>::iterator its;
	for (it = dep->depStudents.begin(); it != dep->depStudents.end(); ++it) {
		for (its = year->yearFlowStudents.begin(); its != year->yearFlowStudents.end(); ++its) {
			if (*it._Ptr == *its._Ptr) {
				started++;
				if ((*it._Ptr)->nekudot_total >= 160) {
					(*it._Ptr)->active = false;
					finished++;
				}
			}
		}
	}
	cout << "Started: " << started << " Finished: " << finished << endl;
}