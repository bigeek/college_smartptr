#include <iostream>
#include <vector>
#include "templateClass.h"
#include "smartPointer.h"
#include "Departament.h"
#include "Course.h"
#include "YearFlow.h"
#include "errorManagment.h"
#pragma once


using namespace std;


class College : public templateClass
{
public:
	vector<Departament*> depts;
	vector<YearFlow*> collYears;
	vector<Course*> collCourses;

	College(const char* nullStr) : templateClass(nullStr){}

	~College(){}

	void CreateCourse(int num);

	void CreateDep(const char* nullStr);

	void CreateStudent(const char* nullStr, const char* lastname, int id, const char* adress_, int start, const char* departament);

	Student* SearchStudent(int num);

	YearFlow* SearchYear(int num);

	Course* SearchCourseByNum(int num);

	Departament* SearchDepartament(const char* departament);

	void EditCourse(int num, const char* departament, double score, const char* nullStr);

	void AddStudentToCourse(Course* course, Student* student);

	void RemoveStudentFromCourse(Course* course, Student* student);

	void RemoveStudent(Student* student, int year);

	void FinishCourseStudent(Course* course, Student* student);

	void FinishCourse(Course* course);

	void PrintCourse(Course* course);

	void PrintYearFlow(Departament* dep, YearFlow* year);
};

