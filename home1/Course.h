#include <iostream>
#pragma once
#include "templateClass.h"
#include "Student.h"
using namespace std;

class Course : public templateClass
{
public:
	double score;
	int course_num;
	vector<Student*> courseStudents;
	Course(int num): templateClass("EMPTY"), score(NULL), course_num(num)
	{}
	~Course() {}
protected:

};