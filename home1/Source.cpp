#define _CRT_SECURE_NO_WARNINGS
#include <fstream>
#include <sstream>
#include "templateClass.h"
#include "College.h"

using namespace std;

vector<string> split(string s, string delimiter) {
	size_t pos_start = 0, pos_end, delim_len = delimiter.length();
	string token;
	vector<string> res;

	while ((pos_end = s.find(delimiter, pos_start)) != string::npos) {
		token = s.substr(pos_start, pos_end - pos_start);
		pos_start = pos_end + delim_len;
		res.push_back(token);
	}

	res.push_back(s.substr(pos_start));
	return res;
}

int main(int argc, char* argv[]) {

	string str, line, tmp;
	size_t pos;


	const char* inp1 = argv[1];
	ifstream data(inp1, ios::in);

	const char* inp2 = argv[2];
	ifstream simu(inp2, ios::in);

	const char* out1 = argv[3];
	ofstream outdata(out1, ios::out);


	istream* input = &cin;
	static ostream* output = &outdata;


	SmartPtr<College> college = new College("Shenkar");

	while (data.eof() != 1) {
		getline(data, line);
		if (line[0] != '#') {
			stringstream(line) >> str;
			string delimiter = ":";
			string token = line.substr(0, line.find(delimiter));

			if (token == "College") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);
				char* cstr0 = new char[v[0].size() + 1];
				copy(v[0].data(), v[0].data() + v[0].size(), cstr0);
				cstr0[v[0].size()] = '\0';
				//	college = new College(cstr0);
			}
			else if (token == "Departments") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);
				for (auto i : v) {
					char* cstr = new char[i.size() + 1];
					copy(i.data(), i.data() + i.size(), cstr);
					cstr[i.size()] = '\0';
					college->CreateDep(cstr);
					*output << "Creating Departament: " << i << endl;
				}
			}
			else if (token == "CollageCoursesList") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);
				for (auto i : v) {
					char* cstr = new char[i.size() + 1];
					copy(i.data(), i.data() + i.size(), cstr);
					cstr[i.size()] = '\0';
					college->CreateCourse(atoi(cstr));
					*output << "Creating Course: " << i << endl;
				}
			}
			else if (token == "CourseDetails") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);


				char* cstr0 = new char[v[0].size() + 1];
				copy(v[0].data(), v[0].data() + v[0].size(), cstr0);
				cstr0[v[0].size()] = '\0';

				char* cstr1 = new char[v[1].size() + 1];
				copy(v[1].data(), v[1].data() + v[1].size(), cstr1);
				cstr1[v[1].size()] = '\0';

				char* cstr2 = new char[v[2].size() + 1];
				copy(v[2].data(), v[2].data() + v[2].size(), cstr2);
				cstr2[v[2].size()] = '\0';

				char* cstr3 = new char[v[3].size() + 1];
				copy(v[3].data(), v[3].data() + v[3].size(), cstr3);
				cstr3[v[3].size()] = '\0';

				college->EditCourse(atoi(cstr0), cstr1, atoi(cstr2), cstr3);
				*output << "Updating Course: " << cstr0 << " " << cstr1 << " " << cstr1 << " " << cstr3 << endl;
			}
			else if (token == "StudentsCreation") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);


				char* cstr0 = new char[v[0].size() + 1];
				copy(v[0].data(), v[0].data() + v[0].size(), cstr0);
				cstr0[v[0].size()] = '\0';

				char* cstr1 = new char[v[1].size() + 1];
				copy(v[1].data(), v[1].data() + v[1].size(), cstr1);
				cstr1[v[1].size()] = '\0';

				char* cstr2 = new char[v[2].size() + 1];
				copy(v[2].data(), v[2].data() + v[2].size(), cstr2);
				cstr2[v[2].size()] = '\0';

				char* cstr3 = new char[v[3].size() + 1];
				copy(v[3].data(), v[3].data() + v[3].size(), cstr3);
				cstr3[v[3].size()] = '\0';

				char* cstr4 = new char[v[4].size() + 1];
				copy(v[4].data(), v[4].data() + v[4].size(), cstr4);
				cstr4[v[4].size()] = '\0';

				char* cstr5 = new char[v[5].size() + 1];
				copy(v[5].data(), v[5].data() + v[5].size(), cstr5);
				cstr5[v[5].size()] = '\0';

				college->CreateStudent(cstr0, cstr1, atoi(cstr2), cstr3, atoi(cstr4), cstr5);
				*output << "Creating Student: " << cstr0 << " " << cstr1 << " " << cstr2 << " " << cstr3 << " " << cstr4 << " " << cstr5 << endl;
			}
			else {
				*output << "parse error";
			}
		}
	}


	while (simu.eof() != 1) {
		getline(simu, line);
		if (line[0] != '#') {
			stringstream(line) >> str;
			string delimiter = ",";
			string token = line.substr(0, line.find(delimiter));
			if (token == "1") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);

				char* cstr0 = new char[v[0].size() + 1];
				copy(v[0].data(), v[0].data() + v[0].size(), cstr0);
				cstr0[v[0].size()] = '\0';

				char* cstr1 = new char[v[1].size() + 1];
				copy(v[1].data(), v[1].data() + v[1].size(), cstr1);
				cstr1[v[1].size()] = '\0';

				char* cstr2 = new char[v[2].size() + 1];
				copy(v[2].data(), v[2].data() + v[2].size(), cstr2);
				cstr2[v[2].size()] = '\0';

				char* cstr3 = new char[v[3].size() + 1];
				copy(v[3].data(), v[3].data() + v[3].size(), cstr3);
				cstr3[v[3].size()] = '\0';

				char* cstr4 = new char[v[4].size() + 1];
				copy(v[4].data(), v[4].data() + v[4].size(), cstr4);
				cstr4[v[4].size()] = '\0';

				char* cstr5 = new char[v[5].size() + 1];
				copy(v[5].data(), v[5].data() + v[5].size(), cstr5);
				cstr5[v[5].size()] = '\0';
				try {
					college->CreateStudent(cstr0, cstr1, atoi(cstr2), cstr3, atoi(cstr4), cstr5);
					*output << "Creating Student: " << cstr0 << " " << cstr1 << " " << cstr2 << " " << cstr3 << " " << cstr4 << " " << cstr5 << endl;
				}
				catch (exception & e) {
					*output << e.what() << endl;
				}
			}
			else if (token == "2") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);

				char* cstr0 = new char[v[0].size() + 1];
				copy(v[0].data(), v[0].data() + v[0].size(), cstr0);
				cstr0[v[0].size()] = '\0';

				char* cstr1 = new char[v[1].size() + 1];
				copy(v[1].data(), v[1].data() + v[1].size(), cstr1);
				cstr1[v[1].size()] = '\0';
				try {
					college->AddStudentToCourse(college->SearchCourseByNum(atoi(cstr1)), college->SearchStudent(atoi(cstr0)));
					*output << "Adding Student ID: " << cstr0 << " To course id: " << cstr0 << endl;
				}
				catch (exception & e) {
					*output << e.what() << endl;
				}
			}
			else if (token == "3") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);

				char* cstr0 = new char[v[0].size() + 1];
				copy(v[0].data(), v[0].data() + v[0].size(), cstr0);
				cstr0[v[0].size()] = '\0';
				try {
					college->FinishCourse(college->SearchCourseByNum(atoi(cstr0)));
					*output << "Finishing Course ID: " << cstr0 << endl;
				}
				catch (exception & e) {
					*output << e.what() << endl;
				}
			}
			else if (token == "4") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);

				char* cstr0 = new char[v[0].size() + 1];
				copy(v[0].data(), v[0].data() + v[0].size(), cstr0);
				cstr0[v[0].size()] = '\0';

				char* cstr1 = new char[v[1].size() + 1];
				copy(v[1].data(), v[1].data() + v[1].size(), cstr1);
				cstr1[v[1].size()] = '\0';
				try {
					college->FinishCourseStudent(college->SearchCourseByNum(atoi(cstr1)), college->SearchStudent(atoi(cstr0)));
					*output << "Student ID: " << cstr0 << " Finishing Course id: " << cstr1 << endl;
				}
				catch (exception & e) {
					*output << e.what() << endl;
				}
			}
			else if (token == "5") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);

				char* cstr0 = new char[v[0].size() + 1];
				copy(v[0].data(), v[0].data() + v[0].size(), cstr0);
				cstr0[v[0].size()] = '\0';

				char* cstr1 = new char[v[1].size() + 1];
				copy(v[1].data(), v[1].data() + v[1].size(), cstr1);
				cstr1[v[1].size()] = '\0';


				try {
					college->RemoveStudentFromCourse(college->SearchCourseByNum(atoi(cstr1)), college->SearchStudent(atoi(cstr0)));
					*output << "Student ID: " << cstr0 << " Removing From Course ID: " << cstr1 << endl;
				}
				catch (exception & e) {
					*output << e.what() << endl;
				}
			}

			else if (token == "6") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);

				char* cstr0 = new char[v[0].size() + 1];
				copy(v[0].data(), v[0].data() + v[0].size(), cstr0);
				cstr0[v[0].size()] = '\0';

				char* cstr1 = new char[v[1].size() + 1];
				copy(v[1].data(), v[1].data() + v[1].size(), cstr1);
				cstr1[v[1].size()] = '\0';

				try {
					college->RemoveStudent(college->SearchStudent(atoi(cstr0)), atoi(cstr1));
					*output << "Student ID: " << cstr0 << " was stoped studies" << endl;
				}
				catch (exception & e) {
					*output << e.what() << endl;
				}
			}

			else if (token == "7") {
			pos = token.size() + 1; tmp = line.substr(pos);
			string delimiter = ",";
			vector<string> v = split(tmp, delimiter);

			char* cstr0 = new char[v[0].size() + 1];
			copy(v[0].data(), v[0].data() + v[0].size(), cstr0);
			cstr0[v[0].size()] = '\0';

			char* cstr1 = new char[v[1].size() + 1];
			copy(v[1].data(), v[1].data() + v[1].size(), cstr1);
			cstr1[v[1].size()] = '\0';

			try {
				college->PrintYearFlow(college->SearchDepartament(cstr0), college->SearchYear(atoi(cstr1)));
			//	*output << "Student ID: " << cstr0 << " was stoped studies" << endl;
			}
			catch (exception & e) {
				*output << e.what() << endl;
			}
			}
			

			else if (token == "8") {
				pos = token.size() + 1; tmp = line.substr(pos);
				string delimiter = ",";
				vector<string> v = split(tmp, delimiter);

				char* cstr0 = new char[v[0].size() + 1];
				copy(v[0].data(), v[0].data() + v[0].size(), cstr0);
				cstr0[v[0].size()] = '\0';

				try {
					college->PrintCourse(college->SearchCourseByNum(atoi(cstr0)));
				}
				catch (exception & e) {
					*output << e.what() << endl;
				}
			}
		}
	}


	*output << "End of programm!" << endl;

}