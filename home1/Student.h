#include <iostream>
#include <string>
#include <vector>
#include <exception>
#include "templateClass.h"
#pragma once

class Student : public templateClass
{
public:
	char* last_name;
	int id_num;
	char* adress;
	int start_year;
	double nekudot_total;
	int end_year;
	bool active;
	Student(const char* nullStr, const char* lastname, int id, const char* adress_, int start)
		: templateClass(nullStr), id_num(id), start_year(start), nekudot_total(0), end_year(0), active(true)
	{
		size_t len;
		len = strlen(lastname);
		last_name = new char[len + 1];
		memcpy(last_name, lastname, len);
		last_name[len] = 0;

		len = strlen(adress_);
		adress = new char[len + 1];
		memcpy(adress, adress_, len);
		adress[len] = 0;
	}
	~Student() {
		delete[] last_name;
		delete[] adress;
	}

};
