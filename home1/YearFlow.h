#include <iostream>
#include <string>
#include <vector>
#include <exception>
#include "templateClass.h"
#include "Student.h"
using namespace std;
#pragma once


class YearFlow {
public: 
	vector<Student*> yearFlowStudents;
    int start_year;
	YearFlow(int year):start_year(year) {}
	~YearFlow(){}
};
