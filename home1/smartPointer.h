#include <iostream>
#include <string>
#include <vector>
#include "templateClass.h"
#pragma once

template<class T>
class SmartPtr
{
public:
	SmartPtr(T* s) : ptr(s)
	{
		ptr->ref();
	}
	SmartPtr(const T& s) : ptr(s.ptr)
	{
		ptr->ref();
	}
	virtual ~SmartPtr()
	{
		if (!ptr->unRef()) delete ptr;
	}
	template<class newType>
	operator SmartPtr<newType>()
	{
		return SmartPtr<newType>(ptr);
	}

	SmartPtr& operator=(const SmartPtr& rhs);

	T& operator *() { return *ptr; }

	T* operator->()
	{
		return ptr;
	};

	operator T* () {
		return ptr;
	}

	operator T& () {
		return *ptr;
	}

protected:
	T* ptr;
};

class RCObject {
public:
	RCObject() : refCount(0), shareable(true) {}
	virtual~RCObject() = 0 {}
	RCObject(const RCObject& rhs) : refCount(0), shareable(true) {}
	RCObject& operator = (const RCObject& rhs) {
		return *this;
	}
	void addReference() {
		++refCount;
	}
	void removeReference() {
		if (--refCount == 0) delete this;
	}
	void markUnshareable() {
		shareable = false;
	}
	bool isShareable() const {
		return shareable;
	}
	bool isShared() const {
		return refCount > 1;
	}
private:
	int refCount;
	bool shareable;
};
template < class T >
class RCPtr {
private:
	T* pointee;
	void init() {
		if (pointee == 0)
			return;
		if (pointee -> isShareable() == false)
			pointee = new T(*pointee);
		pointee -> addReference();
	}
public:
	RCPtr(T * realPtr = 0) : pointee(realPtr) {
		init();
	}
	RCPtr(const RCPtr & rhs) : pointee(rhs.pointee) {
		init();
	}~RCPtr() {
		if (pointee)
			pointee -> removeReference();
	}
	RCPtr& operator = (const RCPtr & rhs) {
		if (pointee != rhs.pointee) {
			if (pointee) {
				pointee -> removeReference();
			}
			pointee = rhs.pointee;
			init();
		}
		return *this;
	}
	T* operator -> () const {
		return pointee;
	}
	T& operator * () const {
		return *pointee;
	}
};
template < class X >
struct nodeType {
	RCPtr < X > Val; //smart Shared pointer to 
	nodeType < X >* next;
	nodeType < X >(RCPtr < X >& nSmartPointer, nodeType < X >* nextNode) : Val(nSmartPointer), next(nextNode) {

	}
	nodeType < X >(X* realPtr, nodeType < X >* nextNode) : Val(realPtr), next(nextNode) {

	}
};