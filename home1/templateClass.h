#include <iostream>
#include <string>
#include <vector>
#pragma once
class templateClass
{
public:
	templateClass(const char* nullStr) : refCount(0)
	{
		len = strlen(nullStr); 	name = new char[len + 1];
		memcpy(name, nullStr, len); name[len] = 0;
	}
	virtual ~templateClass()
	{
		delete[] name;
	}
	void ChangeName(const char* nullStr);

	char* get() { return name; }
	int ref() const { return ++((templateClass*)this)->refCount; }
	int unRef() const { return --((templateClass*)this)->refCount; }
protected:
	char* name;
	size_t len;
	int refCount;
};

